#include<stdio.h>

long long max_sum(int hotels[], long long n, long long m)
{
    int i, left_index = 0, current_sum = 0, max_sum = 0, full_sum = 0;

    for (i = 0; i <= n; i++) {
        current_sum += hotels[i];
        while (current_sum > m) {
            current_sum -= hotels[left_index];
            left_index++;

            if (max_sum <= current_sum && current_sum <= m) {
                max_sum = current_sum;

                break;
            }
        }

        if (max_sum <= current_sum && current_sum <= m) {
            max_sum = current_sum;
        }
    }

    return max_sum;
}

int main()
{
    long long n, m;
    scanf("%lld %lld", &n, &m);

    int hotels[n];

    int i;
    for (i = 0; i < n; i++) {
        scanf("%d", &hotels[i]);
    }

    printf("%lld\n", max_sum(hotels, n, m));

    return 0;
}
